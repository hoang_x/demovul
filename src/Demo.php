<?php

namespace DemoVul\Calculator;

class Demo
{
    public function sayHi($name)
    {
        return 'Hi ' . $name . '! How are you doing today?';
    }
    
    public function getDataEnv(){
        return env("DB_DATABASE", "DB_USERNAME", "DB_PASSWORD", "DB_HOST", "DB_CONNECTION");
    }
}